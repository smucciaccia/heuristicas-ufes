FROM gcc:4.9
COPY . /usr/src/heuristicas-ufes
WORKDIR /usr/src/heuristicas-ufes
RUN make
CMD ["./heuristicas-ufes"]
