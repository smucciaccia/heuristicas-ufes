#include "../src/CVRP.hpp"
#include <GL/freeglut.h>
#include <list>
#include <string>
using namespace std; 

int largura = 501, altura = 501;
int raio = 3;
string titulo = "CVRP solution";

float cores_rgb[8][3] = {{0.0, 0.0, 0.0},
                         {1.0, 0.0, 0.0},
                         {0.0, 1.0, 0.0},
                         {0.0, 0.0, 1.0},
                         {0.0, 1.0, 1.0},
                         {1.0, 0.0, 1.0},
                         {1.0, 1.0, 0.0},
                         {1.0, 1.0, 1.0}};

Grafo *g;
Solucao *s;

struct circulo{
  int x, y, raio, cor;
  circulo(int a, int b, int c, int d): x(a), y(b), raio(c), cor(d) {}
};

struct linha{
  int x1, y1, x2, y2, cor;
  linha(int a, int b, int c, int d, int e): x1(a), y1(b), x2(c), y2(d), cor(e) {}
};

std::list<circulo> listaCirculos;
std::list<linha> listaLinhas;

bool estaDentro(circulo c, int x, int y)
{
  return (x-c.x)*(x-c.x) + (y-c.y)*(y-c.y) <= c.raio * c.raio;
}

bool estaEmAlgumCirculo(int x, int y){
  bool esta = false;
  std::list<circulo>::const_iterator it;
  for (it = listaCirculos.begin(); it != listaCirculos.end(); ++it) {
    circulo aux = *it;
    if ( estaDentro(*it, x, y) )
      esta = true;
  }
  return esta;
}

int acertarX(int x){
  return x;
}

int acertarY(int y){
  return altura -y;
}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT);

  std::list<linha>::const_iterator it;
  for (it = listaLinhas.begin(); it != listaLinhas.end(); ++it) {
    linha li = *it;
    glColor3f(cores_rgb[li.cor][0],cores_rgb[li.cor][1],cores_rgb[li.cor][2]);
    glBegin(GL_LINES);
      glVertex2i(acertarX(li.x1), acertarY(li.y1));
      glVertex2i(acertarX(li.x2), acertarY(li.y2));
    glEnd();
  }

  glColor3f(cores_rgb[0][0],cores_rgb[0][1],cores_rgb[0][2]);
  glBegin(GL_POINTS);
    int i,j;
    for(i=0; i<largura; i++)
      for(j=0; j<altura; j++)
        if( estaEmAlgumCirculo(i,j) ){
          glVertex2i(acertarX(i),acertarY(j));
        }
  glEnd();

  glFlush();
}

void init(void)
{
  /* selecionar cor de fundo (preto) */
  glClearColor(cores_rgb[7][0], cores_rgb[7][1], cores_rgb[7][2], 0.0);

  /* inicializar sistema de viz. */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0.0, (float) largura, (float) altura, 0.0);

  /* sistema de anti-aliasing */
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);
  glHint(GL_LINE_SMOOTH, GL_NICEST);
  glEnable(GL_POINT_SMOOTH);
  glHint(GL_POINT_SMOOTH, GL_NICEST);
}

int main(int argc, char** argv)
{
  if (argc < 3 || argc > 3) { 
    cout << "Exemplo de execução:" << endl; 
    cout << "./plot-solutions ../problem/A-n33-k5.vrp ../solution/A-n33-k5.sol" << endl; 
    return 0;
  }

  Grafo ge(argv[1]);
  Solucao se(ge, argv[2]);
  g = &ge;
  s = &se;

  int i, cor=0;
  vector<int> rota = s->getRota();
  for (i=0; i<rota.size(); i++) {
    if (rota[i]==0) cor++;
    double x1, x2, y1, y2, mul;
    mul = 5;
    x1 = mul * g->getVertice(rota[i]).getX();
    y1 = mul * g->getVertice(rota[i]).getY();
    x2 = mul * g->getVertice(rota[i+1]).getX();
    y2 = mul * g->getVertice(rota[i+1]).getY();
    listaLinhas.push_back(linha(x1, y1, x2, y2, cor));

    circulo c(x1, y1, raio, 0);
    listaCirculos.push_back(c);
  }

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(largura, altura);
  glutInitWindowPosition(100,100);
  glutCreateWindow(titulo.c_str());
  init();
  glutDisplayFunc(display);
  glutMainLoop();

  return 0;
} 
