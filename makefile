all: obj/main.o obj/cvrp.o obj/GeradorDeSolucaoInicial.o obj/GeradorDeSolucaoVizinha.o obj/GeradorDeCrossOver.o obj/HillClimbing.o obj/IteratedLocalSearch.o obj/VariableNeighborhoodSearch.o obj/GeneticAlgorithm.o obj/SimulatedAnnealing.o obj/AntColony.o obj/GRASP.o
	g++ -std=c++11 -pthread -O3 obj/*.o -o heuristicas-ufes
	./heuristicas-ufes -p ./problems/set-a/ -s ./solutions/

obj/main.o: src/main.cpp
	g++ -c -std=c++11 -pthread -O3 src/main.cpp -o obj/main.o

obj/cvrp.o: src/cvrp.cpp src/cvrp.hpp
	g++ -c -std=c++11 -pthread -O3 src/cvrp.cpp -o obj/cvrp.o

obj/GeradorDeSolucaoInicial.o: src/GeradorDeSolucaoInicial.cpp src/GeradorDeSolucaoInicial.hpp
	g++ -c -std=c++11 -pthread -O3 src/GeradorDeSolucaoInicial.cpp -o obj/GeradorDeSolucaoInicial.o

obj/GeradorDeSolucaoVizinha.o: src/GeradorDeSolucaoVizinha.cpp src/GeradorDeSolucaoVizinha.hpp
	g++ -c -std=c++11 -pthread -O3 src/GeradorDeSolucaoVizinha.cpp -o obj/GeradorDeSolucaoVizinha.o

obj/GeradorDeCrossOver.o: src/GeradorDeCrossOver.cpp src/GeradorDeCrossOver.hpp
	g++ -c -std=c++11 -pthread -O3 src/GeradorDeCrossOver.cpp -o obj/GeradorDeCrossOver.o

obj/HillClimbing.o: src/HillClimbing.cpp src/HillClimbing.hpp
	g++ -c -std=c++11 -pthread -O3 src/HillClimbing.cpp -o obj/HillClimbing.o

obj/IteratedLocalSearch.o: src/IteratedLocalSearch.cpp src/IteratedLocalSearch.hpp
	g++ -c -std=c++11 -pthread -O3 src/IteratedLocalSearch.cpp -o obj/IteratedLocalSearch.o

obj/VariableNeighborhoodSearch.o: src/VariableNeighborhoodSearch.cpp src/VariableNeighborhoodSearch.hpp
	g++ -c -std=c++11 -pthread -O3 src/VariableNeighborhoodSearch.cpp -o obj/VariableNeighborhoodSearch.o

obj/GeneticAlgorithm.o: src/GeneticAlgorithm.cpp src/GeneticAlgorithm.hpp
	g++ -c -std=c++11 -pthread -O3 src/GeneticAlgorithm.cpp -o obj/GeneticAlgorithm.o

obj/SimulatedAnnealing.o: src/SimulatedAnnealing.cpp src/SimulatedAnnealing.hpp
	g++ -c -std=c++11 -pthread -O3 src/SimulatedAnnealing.cpp -o obj/SimulatedAnnealing.o

obj/AntColony.o: src/AntColony.cpp src/AntColony.hpp
	g++ -c -std=c++11 -pthread -O3 src/AntColony.cpp -o obj/AntColony.o

obj/GRASP.o: src/GRASP.cpp src/GRASP.hpp
	g++ -c -std=c++11 -pthread -O3 src/GRASP.cpp -o obj/GRASP.o

clean:
	rm obj/*.o
	rm heuristicas-ufes
