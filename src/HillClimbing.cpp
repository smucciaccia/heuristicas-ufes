#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "HillClimbing.hpp"
#include <vector>
using namespace std;

// class HillClimbing -------------------------------------------------------------------

  HillClimbing::HillClimbing(){
    gsv = GeradorDeSolucaoVizinha();
  }

  HillClimbing::HillClimbing(string vizinhanca){
    gsv = GeradorDeSolucaoVizinha(vizinhanca);
  }

  Solucao HillClimbing::executa(Grafo *g){
    GeradorDeSolucaoInicial gsi;
    Solucao si = gsi.gerar(g);
    Solucao s_atual = si; 
    Solucao s_melhor = si;      
    while (gsv.melhorarSolucao(s_atual, s_melhor)) {
      s_atual = s_melhor;
    }
    return s_atual;
  }

  Solucao HillClimbing::executa(Solucao s){
    Solucao s_atual = s; 
    Solucao s_melhor = s;      
    while (gsv.melhorarSolucao(s_atual, s_melhor)) {
      s_atual = s_melhor;
    }
    return s_atual;
  }
  
  void HillClimbing::executa2opt(Solucao& sol){
    GeradorDeSolucaoVizinha2opt gsv;
    int max = sol.getRota().size()-1;
    bool melhorou;
    do {
      melhorou = false;
      for (int d=2; d<max; d++) {
        for (int i=0; i+d<max; i++) {
          if (gsv.dif(sol, i, i+d) && gsv.preValidade(sol, i, i+d)) {
            gsv.move(sol, i, i+d);
            melhorou=true;
          }
        }
      }
    } while (melhorou);
  }

  Solucao HillClimbing::executa2opt(Grafo *g){
    GeradorDeSolucaoInicial gsi;
    Solucao sol = gsi.gerar(g);
    GeradorDeSolucaoVizinha2opt gsv;
    int max = sol.getRota().size()-1;
    bool melhorou;
    do {
      melhorou = false;
      cout << "Custo Atual: " << sol.custo() << endl;
      for (int d=2; d<max; d++) {
        for (int i=0; i+d<max; i++) {
          if (gsv.dif(sol, i, i+d) && gsv.preValidade(sol, i, i+d)) {
            gsv.move(sol, i, i+d);
            melhorou=true;
          }
        }
      }
    } while (melhorou);

    return sol;
  }
