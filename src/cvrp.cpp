#include "cvrp.hpp"
#include <math.h>
#include <vector>
#include <set>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <chrono>
using namespace std;

// class Vertice ----------------------------------------------------------------------------------

  Vertice::Vertice () {
    x=0;
    y=0;
  }

  Vertice::Vertice (float ax, float ay) {
    x=ax;
    y=ay;
  }

  float Vertice::getX() const { return x; }
  float Vertice::getY() const { return y; }

  string Vertice::toString() const {
    stringstream ss;
    ss << "(" << x << "," << y << ")";
    return ss.str();
  }



// class Aresta -----------------------------------------------------------------------------------

  Aresta::Aresta () {
    v1=0;
    v2=0;
  }

  Aresta::Aresta (int av1, int av2, float acomprimento) {
    if(av1<av2) {
      v1=av1;
      v2=av2;
    } else {
      v1 = av2;
      v2 = av1;
    }
    comprimento=acomprimento;
  }
  
  bool Aresta::operator< (const Aresta& a2) const {
    if (comprimento < a2.comprimento)
      return true;
    else if (comprimento > a2.comprimento)
      return false;
    else if(v1 < a2.v1 || v1 == a2.v1 && v2 < a2.v2)
      return true;
    else
      return false;
  }

  bool Aresta::operator== (const Aresta& a2) const {
    return v1==a2.v1 && v2==a2.v2;
  }

  float Aresta::getV1() const { return v1; }
  float Aresta::getV2() const { return v2; }
  float Aresta::getComprimento() const { return comprimento; }

  string Aresta::toString () const {
    stringstream ss;
    ss << "<" << v1 << "," << v2 << ">";
    return ss.str();
  }



// class Grafo ------------------------------------------------------------------------------------

  Grafo::Grafo(){
    cout << "Construtor chamado" << endl;
  }

  Grafo::Grafo(string nomeArquivo) {
    ifstream arquivo;
    arquivo.open(nomeArquivo);
    if(!arquivo){
      cerr << "Não foi possível abrir o arquivo!" << endl;
      exit(1);
    }

    opt = -1;
    while(getline(arquivo, laux)){
      if(laux.compare(0, 4, "NAME") == 0 )
        lname="a";
      if(laux.compare(0, 7, "COMMENT") == 0 ){
        if(laux.find("value:") != string::npos)
          sscanf(laux.substr(laux.find("value:")).c_str(), "value: %d", &opt);
      }
      if(laux.compare(0, 4, "TYPE") == 0 )
        lname="c";
      if(laux.compare(0, 9, "DIMENSION") == 0 )
        sscanf(laux.c_str(), "DIMENSION : %d", &numVertices);
      if(laux.compare(0, 16, "EDGE_WEIGHT_TYPE") == 0 )
        lname="d";
      if(laux.compare(0, 8, "CAPACITY") == 0 )
        sscanf(laux.c_str(), "CAPACITY : %d", &capacity);
      if(laux.compare(0, 18, "NODE_COORD_SECTION") == 0 )
        break;
    }

    int n;
    float a, b;
    for (int i=0; i<numVertices; i++) {
      arquivo >> n;
      arquivo >> a;
      arquivo >> b;
      Vertice p(a,b);
      vertices.push_back(p);
    }

    getline(arquivo, laux2);
    getline(arquivo, laux3);
    int p, c;
    for (int i=0; i<numVertices; i++) {
      arquivo >> p >> c;
      demandas.push_back(c);
    }

    arquivo.close();
  }

  float Grafo::calcularDistancia(int v1, int v2) const {
    float x1, y1, x2, y2;
    x1 = vertices[v1].getX();
    x2 = vertices[v2].getX();
    y1 = vertices[v1].getY();
    y2 = vertices[v2].getY();

    return sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
  }

  float Grafo::distancia(int v1, int v2) const {
    return calcularDistancia(v1, v2);
  }

  int Grafo::demanda(int i) const {
    return demandas[i];
  }

  string Grafo::toString() const {
    stringstream ss;
    ss << "Grafo:" << endl;
    ss << "Capacity=" << capacity << endl;
    for (int i=0; i<vertices.size(); i++) {
      ss << setfill(' ') << setw(2) << i << " ";
      ss << vertices[i].toString() << " demanda: " << demandas[i] << endl;
    }
    return ss.str();
  }

  void Grafo::gerarArestas(){
    arestas = vector<vector<Aresta>>(numVertices);
    for(int i=0; i<numVertices; i++){
      arestas[i] = vector<Aresta>(numVertices);
      for(int j=0; j<numVertices; j++){
        arestas[i][j] = Aresta(i, j, calcularDistancia(i,j));
      }    
    }
  }

  Vertice Grafo::getVertice(int v) const {
    return vertices[v%numVertices];
  }

  Aresta Grafo::getAresta(int v1, int v2) const {
    return arestas[v1][v2];
  }

  int Grafo::getNumVertices() const {
    return numVertices;
  }

  int Grafo::getNumArestas() const {
    return numVertices*(numVertices-1)/2;
  }
  
  int Grafo::getCapacidade() const {
    return capacity;
  }

  int Grafo::getBestValue() const {
    return opt;
  }


// class Solucao ----------------------------------------------------------------------------------

  Solucao::Solucao(){}

  Solucao::Solucao(Grafo *ge, vector<int> rotae){
    g = ge;
    rota = rotae;
  }

  Solucao::Solucao(Grafo *ge, vector<int> rotaSemRep, int a){
    g = ge;
    int capacidade = 0;
    rota.push_back(0);
    for (int i=0; i<rotaSemRep.size(); i++) {
      capacidade += g->demanda(rotaSemRep[i]);
      if (capacidade > 100) {
        rota.push_back(0);
        capacidade = g->demanda(rotaSemRep[i]);
      }
      rota.push_back(rotaSemRep[i]);
    }
    rota.push_back(0);
  }

  Solucao::Solucao(const Solucao& sol){
    g = sol.g;
    rota = sol.rota;
  }

  Solucao::Solucao(Grafo *ge, string nomeArquivo){
    g = ge;
    ifstream arquivo;
    arquivo.open(nomeArquivo);
    if(!arquivo){
      cerr << "Não foi possível abrir o arquivo de solução!" << endl;
      exit(1);
    }

    string linha, ignorar;
    while (getline(arquivo, linha) && linha.rfind("Route") == 0) {
      rota.push_back(0);
      stringstream ss(linha);
      ss >> ignorar >> ignorar;
      int n;
      while (ss >> n) {
        rota.push_back(n);
      }
    }
    rota.push_back(0);
    arquivo.close();
  }

  bool Solucao::operator< (const Solucao& s2) const {
    if (numeroCaminhoes() < s2.numeroCaminhoes()) 
      return true;
    else if (numeroCaminhoes() > s2.numeroCaminhoes()) 
      return false;
    else {
      if (custo() < s2.custo())
        return true;
      else
        return false;
    }
  }

  int Solucao::compare (const Solucao& s2) const {
    multiset<Aresta> mset;
    for(int i=0; i<rota.size()-1; i++)
      if(rota[i]!=0 || rota[i+1]!=0)
        mset.insert( Aresta(rota[i], rota[i+1], g->distancia(rota[i], rota[i+1])) );
    for(int i=0; i<s2.rota.size()-1; i++)
      if(s2.rota[i]!=0 || s2.rota[i+1]!=0)
        mset.insert( Aresta(s2.rota[i], s2.rota[i+1], g->distancia(s2.rota[i], s2.rota[i+1])) );
    int cont = 0;
    multiset<Aresta>::iterator it;
    for (it=mset.begin(); it!=mset.end(); ++it)
      if(mset.count(*it) == 1) 
        cont++;
    return cont;
  }

  int Solucao::similaridade(const Solucao& s2) const {
    multiset<Aresta> mset;
    for(int i=0; i<rota.size()-1; i++)
      if(rota[i]!=0 || rota[i+1]!=0)
        mset.insert( Aresta(rota[i], rota[i+1], g->distancia(rota[i], rota[i+1])) );
    for(int i=0; i<s2.rota.size()-1; i++)
      if(s2.rota[i]!=0 || s2.rota[i+1]!=0)
        mset.insert( Aresta(s2.rota[i], s2.rota[i+1], g->distancia(s2.rota[i], s2.rota[i+1])) );
    int cont = 0;
    multiset<Aresta>::iterator it;
    for (it=mset.begin(); it!=mset.end(); ++it)
      if(mset.count(*it) == 2) 
        cont++;
    return cont/2;
  }

  bool Solucao::valida() const {
    int soma = 0;
    for (int i=0; i<rota.size(); i++) {
      if (soma > g->getCapacidade()) return false;
      if (rota[i]==0) soma = 0;
      soma += g->demanda(rota[i]);
    }
    return true;
  }

  int Solucao::custo() const {
    return distanciaTotalInteira();
  }

  int Solucao::numeroCaminhoes() const {
    int soma = 1;
    for (int i=1; i<rota.size()-1; i++) {
      if(rota[i]==0 && rota[i-1]!=0) soma++;
    }
    return soma;
  }

  int Solucao::distanciaTotalInteira() const {
    int soma = 0;
    for (int i=0; i<rota.size()-1; i++) {
      soma += round( g->distancia(rota[i], rota[i+1]) );
    }
    soma += round( g->distancia(rota[rota.size()-1], rota[0]) );
    return (int) soma;
  }

  float Solucao::distanciaTotal() const {
    float soma = 0;
    for (int i=0; i<rota.size()-1; i++) {
      soma += g->distancia(rota[i], rota[i+1]);
    }
    soma += g->distancia(rota[rota.size()-1], rota[0]);
    return soma;
  }

  float Solucao::distanciaDaMaiorRota() const {
    float maiorDistancia = 0;
    float soma = 0;
    for (int i=0; i<rota.size()-1; i++) {
      if (rota[i]==0) {
        if (maiorDistancia < soma) maiorDistancia = soma;
        soma = 0; 
      }
      soma += g->distancia(rota[i], rota[i+1]);
    }
    soma += g->distancia(rota[rota.size()-1], rota[0]);
    return maiorDistancia;
  }

  void Solucao::acertarZerosRepetidos() {
    int rep = 0;
    for (int i=0; (i+rep)<rota.size(); i++) {
      if (i != 0 && rota[i-1] == 0) {
        while (rota[i+rep]==0) {
          rep++;
        }
      }
      rota[i]=rota[i+rep];
    }
    rota.resize(rota.size() - rep);
  }

  string Solucao::toString() const {
    stringstream ss;
    bool primeiro=false;
    int n=0;
    for (int i=0; i<rota.size(); i++) {
      if (rota[i]==0) {
        n++;
        if (i!=0) ss << endl;
        if (i!=rota.size()-1) ss << "Route #" << n << ": "; 
        primeiro=true;
      } else {
        if (primeiro==true) {
          primeiro = false;
        } else {
          ss << " ";
        }
        ss << rota[i];
      }
    }
    ss << "cost " << custo() << endl;;
    return ss.str();
  }
  
  string Solucao::serialize() const {
    stringstream ss;
    for (int i=0; i<rota.size(); i++) {
      ss << rota[i];
      if (i < rota.size()-1) ss << ",";
    }
    return ss.str();
  }

  string Solucao::analisaDistancia() const {
    stringstream ss;
    ss << "Distância da maior rota: " << distanciaDaMaiorRota() << endl;;
    ss << "Distância total: " << distanciaTotal() << endl;;
    return ss.str();
  }
  
  string Solucao::analisa() const {
    stringstream ss;
    bool primeiro=false;
    int n=0;
    int somaDemandas = 0;
    float somaDistancias = 0;
    for (int i=0; i<rota.size(); i++) {
      somaDemandas += g->demanda(rota[i]);
      if (i!=rota.size()-1) {
        somaDistancias += g->distancia(rota[i], rota[i+1]);
      }
      if (rota[i]==0) {
        n++;
        if (i!=0){
          ss << "    Capacidade: " << somaDemandas << "    Distancia: " << somaDistancias << endl;
          somaDemandas = 0;
          somaDistancias = 0;
        }
        if (i!=rota.size()-1) {
          ss << "Route #" << n << ": "; 
        }
        primeiro=true;
      } else {
        if (primeiro==true) {
          primeiro = false;
        } else {
          ss << " ";
        }
        ss << rota[i];
      }
    }
    ss << "cost " << custo() << endl;;
    return ss.str();
  }

  vector<int> Solucao::getRota() const{
    return rota;
  }

  int Solucao::getRota(int v) const{
    return rota[v];
  }

  unsigned int Solucao::distancia(int r1, int r2) const{
    return (unsigned int) round(g->distancia(rota[r1], rota[r2]));
  }
  
  vector<int> Solucao::getRotaSemRep() const{
    vector<int> rotaSemRep;
    for(int i=0; i<rota.size(); i++) 
      if(rota[i]!=0)
        rotaSemRep.push_back(rota[i]);
    return rotaSemRep;
  }

  Grafo* Solucao::getGrafo() {
    return g;
  }

// class CriterioDeParada -------------------------------------------------------------------------

  CriterioDeParada::CriterioDeParada(){}

  void CriterioDeParada::setTempoLimite(int limite){
    limiteDeTempo = limite;
  }

  void CriterioDeParada::setCustoAlvo(int alvo){
    custoAlvo = alvo;
  }

  void CriterioDeParada::registraCusto(int custo){
    custoRegistrado = custo;
    tempoCusto = chrono::high_resolution_clock::now();
  }

  void CriterioDeParada::start(){
    inicio = chrono::high_resolution_clock::now();
  }

  void CriterioDeParada::stop(){
    fim = chrono::high_resolution_clock::now();
  }
  
  int CriterioDeParada::tempoSegundos(){
    chrono::seconds time_sec = chrono::duration_cast<chrono::seconds>(fim - inicio);
    return time_sec.count();
  }

  int CriterioDeParada::tempoCustoSegundos(){
    chrono::seconds time_sec = chrono::duration_cast<chrono::seconds>(tempoCusto - inicio);
    return time_sec.count();
  }

  bool CriterioDeParada::parar(){
    if (limiteDeTempo > 0) {
      chrono::high_resolution_clock::time_point agora = chrono::high_resolution_clock::now();
      chrono::seconds time_sec = chrono::duration_cast<chrono::seconds>(agora - inicio);
      int t_agora = time_sec.count();
      if (t_agora >= limiteDeTempo)
        return true;
    }
    if (custoRegistrado >= 0) {
      if ( custoRegistrado <= custoAlvo )
        return true;
    }
    return false;
  }



// class CalculadorDeLowerBound -------------------------------------------------------------------

  CalculadorDeLowerBound::CalculadorDeLowerBound(){}

  float CalculadorDeLowerBound::executa(Grafo *g){
    for(unsigned int i=0; i<g->getNumVertices(); i++){
      for(unsigned int j=i+1; j<g->getNumVertices(); j++){
        Aresta aresta(i, j, g->distancia(i, j));
        vetorArestas.push_back(aresta);
      }
    }
    sort(vetorArestas.begin(), vetorArestas.end());
    unsigned int numRotas = 5;
    for(unsigned int i=0; i<g->getNumVertices(); i++){
      unsigned int a=0, n=0;
      while(n<2){
        if(vetorArestas[a].getV1() == i || vetorArestas[a].getV2() == i){
          setArestas.insert(vetorArestas[a]);
          n++;
        }
        a++;
      }
    }
    float soma = 0;
    unsigned int na=0;
    for (set<Aresta>::iterator it=setArestas.begin(); it!=setArestas.end(); ++it){
      soma += (*it).getComprimento();
      na++;
      if(na >= g->getNumVertices() + numRotas - 1) break;
    }
    return soma;  
  }

// class Logger -----------------------------------------------------------------------------------

  Logger::Logger(){
    int n=0;
    string nomeLog;
    do {
      n++;
      nomeLog.clear();
      nomeLog.append("./logs/log").append(to_string(n)).append(".csv");
    } while (ifstream(nomeLog.c_str()).good());

    log.open(nomeLog);
    if(!log){
      cerr << "Não foi possível abrir o arquivo para escrever o log!" << endl;
      exit(1);
    }
  }

  Logger::~Logger(){
    log.close();
  }

  void Logger::registra(string problema){
    log << endl << problema.substr(problema.rfind("/") + 1) << ", ";
    log.flush();
  }

  void Logger::registra(Grafo *g, int custo, int tempo){
    log << custo << ", " << tempo << ", ";
    log.flush();
    soma += custo - g->getBestValue();
  }
