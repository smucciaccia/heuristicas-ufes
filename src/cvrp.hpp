#ifndef CVRP
#define CVRP

#include <math.h>
#include <vector>
#include <set>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <chrono>
using namespace std;

class Vertice
{
  public:
    float x,y;
    Vertice();
    Vertice (float ax, float ay);
    float getX() const;
    float getY() const;
    string toString() const;
};

class Aresta
{

  public:
    int v1,v2;
    float comprimento;

    Aresta ();
    Aresta (int av1, int av2, float acomprimento);

    bool operator< (const Aresta& a2) const;
    bool operator== (const Aresta& a2) const;
    float getV1() const;
    float getV2() const;
    float getComprimento() const;
    string toString () const;
};

class Grafo
{
  private:
    string lname, lcomment, ltype, ldimension, ledge_weight_type, lcapacity, laux, laux2, laux3;
    unsigned int numVertices, dimension, capacity;
    int opt;
    vector<Vertice> vertices;
    vector<int> demandas;
    vector<vector<Aresta>> arestas;

  public:
    Grafo();

    Grafo(string nomeArquivo);

    float calcularDistancia(int v1, int v2) const;

    float distancia(int v1, int v2) const;

    int demanda(int i) const;

    string toString() const;

    void gerarArestas();

    Vertice getVertice(int v) const;

    Aresta getAresta(int v1, int v2) const;

    int getNumVertices() const;

    int getNumArestas() const;
    
    int getCapacidade() const;

    int getBestValue() const;
};

class Solucao
{
  public:
    Grafo *g;
    vector<int> rota;

    Solucao();

    Solucao(Grafo *ge, vector<int> rotae);

    Solucao(Grafo *ge, vector<int> rotaSemRep, int a);

    Solucao(const Solucao& sol);

    Solucao(Grafo *ge, string nomeArquivo);

    bool operator< (const Solucao& s2) const;

    int compare (const Solucao& s2) const;

    int similaridade (const Solucao& s2) const;

    bool valida() const;

    int custo() const;

    int numeroCaminhoes() const;

    int distanciaTotalInteira() const;

    float distanciaTotal() const;

    float distanciaDaMaiorRota() const;

    void acertarZerosRepetidos();

    string toString() const;
    
    string serialize() const;

    string analisaDistancia() const;
    
    string analisa() const;

    vector<int> getRota() const;

    int getRota(int v) const;

    unsigned int distancia(int r1, int r2) const;
    
    vector<int> getRotaSemRep() const;

    Grafo* getGrafo();

};

/*
 * Implementar no futuro
class SolucaoAresta
{
  public:
    Grafo *g;
    set<Aresta> setAresta;

    SolucaoAresta();

    SolucaoAresta(const Solucao& sol);

    bool operator< (const SolucaoAresta& s2) const;

    int custo() const;

    string toString() const;

    Grafo* getGrafo();
};
*/

class CriterioDeParada
{
  private:
    chrono::high_resolution_clock::time_point inicio;
    chrono::high_resolution_clock::time_point fim;
    chrono::high_resolution_clock::time_point tempoCusto;
    int custoRegistrado = -1;
    int limiteDeTempo = -1;
    int custoAlvo = -1;

  public:
    CriterioDeParada();

    void setTempoLimite(int limite);

    void setCustoAlvo(int alvo);

    void registraCusto(int custo);

    void start();

    void stop();
    
    int tempoSegundos();

    int tempoCustoSegundos();

    bool parar();
};

class CalculadorDeLowerBound
{
  public:
    vector<Aresta> vetorArestas;
    set<Aresta> setArestas;

    CalculadorDeLowerBound();
    float executa(Grafo *g);
};

class Logger
{
  public:
    ofstream log;
    string dirArquivo;
    string nomeArquivo;
    int soma=0;

    Logger();
    ~Logger();

    void registra(string problema);
    void registra(Grafo *g, int custo, int tempo);
};
#endif
