#include "cvrp.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include <vector>
using namespace std;

// class GeradorDeSolucaoVizinha ------------------------------------------------------------------

  GeradorDeSolucaoVizinha::GeradorDeSolucaoVizinha(){}

  GeradorDeSolucaoVizinha::GeradorDeSolucaoVizinha(string tipo){
    if (tipo.compare("2-opt") == 0) {
      modo = 2;
    } else if (tipo.compare("3-opt") == 0) {
      modo = 3;
    } else if (tipo.compare("4-opt") == 0) {
      modo = 4;
    } else {
      modo = 0;
    }
  }

/* Retorna um vizinho com um custo menor que a solução atual.
 * Caso exista um vizinho com um custo menor retorna true e coloca o vizinho
 * encontrado na variável do segundo parâmetro.
 *
 * Entrada: 
 *   (Solucao) Solução atual
 * Saídas: 
 *   (bool) true se conseguiu encontrar uma solução melhor, false caso seja um mínimo local
 *   (Solucao&) A solução melhor encontrada, se não for encontrada não modifica a variável
 */
  bool GeradorDeSolucaoVizinha::melhorarSolucao(Solucao s_atual, Solucao& s_melhor){
    bool melhorou;
    if(modo == 2)
      melhorou = melhorar2opt(s_atual, s_melhor); 
    else if(modo == 3)
      melhorou = melhorar3opt(s_atual, s_melhor); 
    else if(modo == 4)
      melhorou = melhorar4opt(s_atual, s_melhor); 
    else
      melhorou = melhorar2opt(s_atual, s_melhor); 
    return melhorou;
  }

  Solucao GeradorDeSolucaoVizinha::randomMove2opt(Solucao& s_atual){
    int tam = s_atual.getRota().size()-1;
    int i, j;
    do {
      i = rand() % (tam-2);
      j = i+2 + rand() % (tam-i-2);
    } while( !GeradorDeSolucaoVizinha::preValidade2opt(s_atual.getGrafo(), s_atual.getRota(), i, j) );
    
    return v2opt(s_atual, i, j);
  }

  Solucao GeradorDeSolucaoVizinha::v2opt(Solucao sol, int i, int j){
    vector<int> rota = sol.getRota();
    reverse(rota.begin()+i+1, rota.begin()+j+1);
    return Solucao(sol.getGrafo(), rota);
  }
  
  bool GeradorDeSolucaoVizinha::dif2opt(const Solucao sol, int i, int j){
    return sol.distancia(i, j) + sol.distancia(i+1, j+1) < sol.distancia(i, i+1) + sol.distancia(j, j+1);
  }

  int GeradorDeSolucaoVizinha::preCusto2opt(Grafo *g, const vector<int>& rota, int i, int j) const{
    return - (int) round(g->distancia(rota[i], rota[j]))
           - (int) round(g->distancia(rota[i+1],rota[j+1]))
           + (int) round(g->distancia(rota[i], rota[i+1]))
           + (int) round(g->distancia(rota[j],rota[j+1]));
  }

  bool GeradorDeSolucaoVizinha::preValidade2opt(Grafo *g, const vector<int>& rota, int ci, int cj) const{
    if (rota[0]!=0 || rota[rota.size()-1]!=0)
      return false;
    int n, i, j;
    if (ci<cj) {i=ci; j=cj;}
    else {i=cj; j=ci;}
    int a1=0, a2=0, b1=0, b2=0;
    for (n=i; rota[n]!=0; n--)
      a1+=g->demanda(rota[n]);
    for (n=i+1; rota[n]!=0; n++) {
      if (n==j)
        return true;
      a2+=g->demanda(rota[n]);
    }
    for (n=j; rota[n]!=0; n--)
      b1+=g->demanda(rota[n]);
    for (n=j+1; rota[n]!=0; n++)
      b2+=g->demanda(rota[n]);
    return a1+b1 <= g->getCapacidade() && a2+b2 <= g->getCapacidade();
  }

  Solucao GeradorDeSolucaoVizinha::v3opt(Solucao& sol, int i, int j, int k, int n){
    vector<int> rota = sol.getRota();
    if(n==0 || n==1 || n==3)
      reverse(rota.begin()+i+1, rota.begin()+j+1);
    if(n==0 || n==2 || n==3)
      reverse(rota.begin()+j+1, rota.begin()+k+1);
    if(n==1 || n==2 || n==3)
      reverse(rota.begin()+i+1, rota.begin()+k+1);
    return Solucao(sol.getGrafo(), rota);
  }

  Solucao GeradorDeSolucaoVizinha::v4opt(Solucao& sol, int i, int j, int k, int l, int n){
    vector<int> rota = sol.getRota();
    if(n>>0 & 1)
      reverse(rota.begin()+i+1, rota.begin()+j+1);
    if(n>>1 & 1)
      reverse(rota.begin()+j+1, rota.begin()+k+1);
    if(n>>2 & 1)
      reverse(rota.begin()+k+1, rota.begin()+l+1);
    if(n>>3 & 1)
      reverse(rota.begin()+i+1, rota.begin()+k+1);
    if(n>>4 & 1)
      reverse(rota.begin()+j+1, rota.begin()+l+1);
    if(n>>5 & 1)
      reverse(rota.begin()+i+1, rota.begin()+l+1);
    return Solucao(sol.getGrafo(), rota);
  }

  bool GeradorDeSolucaoVizinha::melhorar2opt(Solucao& sol, Solucao& melhor){
    melhor = sol;
    int max = sol.getRota().size()-1;
    for (int i=0; i<max; i++) {
      for (int j=i+2; j<max; j++) {
        if (dif2opt(sol, i, j)) {
          Solucao vizinha = v2opt(sol, i, j);
          if (vizinha.valida()) {
            melhor = vizinha;
            return true;
          }
        }
      }
    }
    return false;
  }

  bool GeradorDeSolucaoVizinha::melhorar3opt(Solucao& sol, Solucao& melhor){
    melhor = sol;
    int max = sol.getRota().size()-1;
    for (int i=1; i<max; i++) {
      for (int j=i+2; j<max; j++) {
        Solucao vizinha = v2opt(sol, i, j);
        if (vizinha.custo() < sol.custo() && vizinha.valida()) {
          melhor = vizinha;
          return true;
        }
      }
    }
    for (int i=0; i<max; i++) {
      for (int j=i+2; j<max; j++) {
        for (int k=j+2; k<max; k++) {
          for (int n=0; n<4; n++) {
            Solucao vizinha = v3opt(sol, i, j, k, n);
            if (vizinha.custo() < sol.custo() && vizinha.valida()) {
              melhor = vizinha;
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  bool GeradorDeSolucaoVizinha::melhorar4opt(Solucao& sol, Solucao& melhor){
    melhor = sol;
    int max = sol.getRota().size()-1;
    for (int i=0; i<max; i++) {
      for (int j=i+2; j<max; j++) {
        for (int k=j+2; k<max; k++) {
          for (int l=k+2; l<max; l++) {
            for (int n=0; n<64; n++) {
              Solucao vizinha = v4opt(sol, i, j, k, l, n);
              if (vizinha.custo() < sol.custo() && vizinha.valida()) {
                melhor = vizinha;
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }



// class GeradorDeSolucaoVizinha2opt --------------------------------------------------------------

  GeradorDeSolucaoVizinha2opt::GeradorDeSolucaoVizinha2opt(){}

  bool GeradorDeSolucaoVizinha2opt::firstGain(Solucao& sol, Solucao& melhor){
    melhor = sol;
    int max = sol.getRota().size()-1;
    for (int i=0; i<max; i++) {
      for (int j=i+2; j<max; j++) {
        if (GeradorDeSolucaoVizinha2opt::preValidade(sol.getGrafo(), sol.getRota(), i, j) && dif(sol, i, j)) {
          move(sol, i, j);
          return true;
        }
      }
    }
    return false;
  }

  void GeradorDeSolucaoVizinha2opt::randomMove(Solucao& s_atual){
    int tam = s_atual.getRota().size()-3;
    int i, j;
    do {
      i = rand() % tam;
      j = rand() % tam-3;
      if (j>i-2) j+=3;
    } while( !GeradorDeSolucaoVizinha2opt::preValidade(s_atual.getGrafo(), s_atual.getRota(), i, j) );
    move(s_atual, i, j);
  }

  void GeradorDeSolucaoVizinha2opt::randomMove(Solucao& s_atual, int n){
    vector<int> vet;
    int max = s_atual.getRota().size()-2;
    for(int i=0; i<n; i++){
      int k;
      do {
        k = rand() % max;
      } while ( find(vet.begin(), vet.end(), k) != vet.end() );
      vet.push_back(k);
    }

    int tam = vet.size();
    for(int k=0; k<n; k++) {
      int i, j, m=n;
      do {
        i = rand() % tam;
        j = rand() % tam-1;
        if (i==j) j++;
        i=vet[i];
        j=vet[j];
        if (GeradorDeSolucaoVizinha2opt::preValidade(s_atual.getGrafo(), s_atual.getRota(), i, j)) {
          move(s_atual, i, j);
          break;
        }
      } while(m--);
    }
  }

  void GeradorDeSolucaoVizinha2opt::move(Solucao& sol, int i, int j){
    reverse(sol.rota.begin()+i+1, sol.rota.begin()+j+1);
  }
  
  bool GeradorDeSolucaoVizinha2opt::dif(const Solucao& sol, int i, int j){
    return sol.distancia(i, j) + sol.distancia(i+1, j+1) < sol.distancia(i, i+1) + sol.distancia(j, j+1);
  }

  int GeradorDeSolucaoVizinha2opt::preCusto(Grafo *g, const vector<int>& rota, int i, int j) const{
    return - (int) round(g->distancia(rota[i], rota[j]))
           - (int) round(g->distancia(rota[i+1],rota[j+1]))
           + (int) round(g->distancia(rota[i], rota[i+1]))
           + (int) round(g->distancia(rota[j],rota[j+1]));
  }

  bool GeradorDeSolucaoVizinha2opt::preValidade(Grafo *g, const vector<int>& rota, int ci, int cj) const{
    if (rota[0]!=0 || rota[rota.size()-1]!=0)
      return false;
    int n, i, j;
    if (ci<cj) {i=ci; j=cj;}
    else {i=cj; j=ci;}
    int a1=0, a2=0, b1=0, b2=0;
    for (n=i; rota[n]!=0; n--)
      a1+=g->demanda(rota[n]);
    for (n=i+1; rota[n]!=0; n++) {
      if (n==j)
        return true;
      a2+=g->demanda(rota[n]);
    }
    for (n=j; rota[n]!=0; n--)
      b1+=g->demanda(rota[n]);
    for (n=j+1; rota[n]!=0; n++)
      b2+=g->demanda(rota[n]);
    return a1+b1 <= g->getCapacidade() && a2+b2 <= g->getCapacidade();
  }

  bool GeradorDeSolucaoVizinha2opt::preValidade(Solucao& sol, int ci, int cj) const{
    if (sol.rota[0]!=0 || sol.rota[sol.rota.size()-1]!=0)
      return false;
    int n, i, j;
    if (ci<cj) {i=ci; j=cj;}
    else {i=cj; j=ci;}
    int a1=0, a2=0, b1=0, b2=0;
    for (n=i; sol.rota[n]!=0; n--)
      a1+=sol.g->demanda(sol.rota[n]);
    for (n=i+1; sol.rota[n]!=0; n++) {
      if (n==j)
        return true;
      a2+=sol.g->demanda(sol.rota[n]);
    }
    for (n=j; sol.rota[n]!=0; n--)
      b1+=sol.g->demanda(sol.rota[n]);
    for (n=j+1; sol.rota[n]!=0; n++)
      b2+=sol.g->demanda(sol.rota[n]);
    return a1+b1 <= sol.g->getCapacidade() && a2+b2 <= sol.g->getCapacidade();
  }
