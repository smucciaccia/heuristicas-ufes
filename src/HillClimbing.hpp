#ifndef H_HILL_CLIMBING
#define H_HILL_CLIMBING

#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include <vector>
using namespace std;

class HillClimbing
{
  private:
    GeradorDeSolucaoVizinha gsv;

  public:
    HillClimbing();
    HillClimbing(string vizinhanca);

    Solucao executa(Grafo *g);
    Solucao executa(Solucao s);
    void executa2opt(Solucao& sol);
    Solucao executa2opt(Grafo *g);
};
#endif
