#include "cvrp.hpp"
#include "GeradorDeCrossOver.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>
using namespace std;

// class GeradorDeCrossOver -----------------------------------------------------------------------

  GeradorDeCrossOver::GeradorDeCrossOver(){
    //srand( unsigned(time(0)) );
    tipo=0;
  }

  // Partially Mapped Crossover (PMX)
  Solucao GeradorDeCrossOver::pmx(Solucao p1, Solucao p2){
    vector<int> rota1 = p1.getRotaSemRep();
    vector<int> rota2 = p2.getRotaSemRep();

    int tam = rota1.size()-1;
    int c1 = rand() % tam;
    int c2 = rand() % tam;

    for (int i=c1; i<=c2; i++) {
      for (int j=0; j<rota2.size(); j++) {
        if (rota2[i]==rota1[j]) {
          int aux = rota1[i];
          rota1[i] = rota1[j];
          rota1[j] = aux;
          break;
        }
      }
    }
    return Solucao(p1.getGrafo(), rota1, 0);
  }
