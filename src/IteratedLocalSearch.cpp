#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "HillClimbing.hpp"
#include "IteratedLocalSearch.hpp"
#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include <functional>
using namespace std;

// class IteratedLocalSearch -------------------------------------------------------------------

  IteratedLocalSearch::IteratedLocalSearch(){}

  Solucao IteratedLocalSearch::executa(Solucao& s_inicial, CriterioDeParada& criterioDeParada){
    int minimosLocais = 0;
    s_melhor = s_inicial;
    function<void(Grafo*)> thread1 = [&](Grafo *g){
      GeradorDeSolucaoVizinha2opt gsv;
      HillClimbing hillClimbing = HillClimbing();
      mtx.lock();
        Solucao s_atual = s_melhor;
      mtx.unlock();
      bool continuar = true;
      while(continuar) {
        gsv.randomMove(s_atual, tamPerturbacao);
        hillClimbing.executa2opt(s_atual);

        mtx.lock();
          minimosLocais++;
          if(s_atual < s_melhor) {
            cout << "Minimos locais explorados: " << minimosLocais;
            cout << ",\tCusto da Solucao Atual: " << s_atual.custo() << endl;
            s_melhor = s_atual;
            criterioDeParada.registraCusto(s_melhor.custo());
          } else {
            s_atual = s_melhor;
          }
          if (criterioDeParada.parar()) continuar=false;
        mtx.unlock();
      }
    };
    vector<thread> threads;
    for (int i=0; i<numThreads; i++) {
      threads.push_back(thread(thread1, s_inicial.getGrafo()));
    }
    for (int i=0; i<numThreads; i++) {
      threads[i].join();
    }
    cout << "Terminou" << endl;
    return s_melhor;
  }
  
  Solucao IteratedLocalSearch::executa(Grafo *g, CriterioDeParada& criterioDeParada){
    GeradorDeSolucaoInicial gsi;
    Solucao s_inicial = gsi.gerar(g);
    return executa(s_inicial, criterioDeParada);
  }

  Solucao IteratedLocalSearch::executa(Grafo *g){
    CriterioDeParada cp;
    cp.setTempoLimite(10);
    return executa(g, cp);
  }
