#ifndef H_GENETIC_ALGORITHM
#define H_GENETIC_ALGORITHM

#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "GeradorDeCrossOver.hpp"
#include "HillClimbing.hpp"
#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include <stddef.h>

//returns a float between 0 & 1
#define NUM_ALEAT      ((float) rand()/((float) RAND_MAX+1))

class GeneticAlgorithm
{
  private:
    Solucao s_melhor;

    // Parametros do Algoritmo Gen�tico
    int tamanhoPop = 100;
    int maxGeracoes = 10000;
    double taxaReproducao = 0.9;
    double taxaMutacao = 0.9;
    double taxaElitismo = 0.1;
    int tamanhoTorneio = 4;

  public:

    GeneticAlgorithm();

    Solucao executa(Grafo *g);

    Solucao executa(Grafo *g, unsigned int alvo);

  private:

    Solucao *GerarPop(Grafo *g, Solucao populacao[]) ;

    Solucao *Selecao(Solucao array_pais[], Solucao populacao[],double somaCumulativa[]) ;

    Solucao *SelecaoTorneio(Solucao array_pais[], Solucao populacao[]) ;

    Solucao *Reproducao(Solucao array_filhos[], Solucao pais[], double taxaReproducao) ;

    Solucao *Mutacao(Solucao array_mutantes[], Solucao individuos[], double taxaMutacao) ;

    Solucao *Elitismo(Solucao populacao[], Solucao nova_pop[], double taxaElitismo,int tamanhoElite) ;

    double *calcSomaCumulativa(double somaCumulativa[], Solucao populacao[]);

    double *calcMetricas(Solucao populacao[],double array_metricas[]);

    int *kMaiores(Solucao populacao[],int k);

    Solucao quick_select_criada(Solucao* input, int p, int r, int k);

    int partition_criada(Solucao* input, int p, int r, int pivot);
};


    #endif
