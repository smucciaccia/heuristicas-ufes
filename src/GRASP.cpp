#include "cvrp.hpp"
#include "HillClimbing.hpp"
#include "GRASP.hpp"
#include <vector>
#include <set>
#include <math.h>
using namespace std;

// class GRASP -------------------------------------------------------------------

  GRASP::GRASP(){}

  int GRASP::roleta(Grafo *g, int cidade, const multiset<int>& cidadesNaoVisitadas, int demanda, float alfa) {
    vector<int> RCL;
    if (cidade!=0)
      RCL.push_back(0);
    for (int i=1; i<g->getNumVertices(); i++) 
      if (i!=cidade && (demanda + g->demanda(i) <= 100) && cidadesNaoVisitadas.count(i)>0)
        RCL.push_back(i);
    for(int i=0; i<RCL.size(); i++)
      for(int j=i+1; j<RCL.size(); j++)
        if(g->distancia(cidade, i) > g->distancia(cidade, j)) {
          int aux = RCL[i];
          RCL[i] = RCL[j];
          RCL[j] = aux;
        }
    if (RCL.size() == 1){
      int saida = RCL[0];
      return saida;
    } else {
      float fsize = (float) RCL.size();
      float fmax = alfa * fsize;
      int max = (int) ceil(fmax);
      int p = rand() % max;
      int saida = RCL[p];
      return saida;
    }
  }

  Solucao GRASP::andar(Grafo *g, float alfa){
    int demanda=0; 
    vector<int> cidadesVisitadas;
    multiset<int> cidadesNaoVisitadas;
    for(int i=1; i<g->getNumVertices(); i++)
      cidadesNaoVisitadas.insert(i);
    cidadesVisitadas.push_back(0);
    while (!cidadesNaoVisitadas.empty()) {
      int cidade = roleta(g, cidadesVisitadas.back(), cidadesNaoVisitadas, demanda, alfa);
      if (cidade==0) {
        demanda = 0;
      } else {
        demanda += g->demanda(cidade);
        cidadesNaoVisitadas.erase(cidadesNaoVisitadas.find(cidade));
      }
      cidadesVisitadas.push_back(cidade);
    } 
    cidadesVisitadas.push_back(0);
    return Solucao(g, cidadesVisitadas);
  }

  Solucao GRASP::executa(Grafo *g, CriterioDeParada& p){
    float alfa=0.1;
    Solucao s_melhor;
    HillClimbing hc;
    p.setTempoLimite(3);
    bool primeiro = true;
    while (true) {
      Solucao s = andar(g, alfa);
      hc.executa2opt(s);
      if (primeiro || s.custo() < s_melhor.custo()) {
        primeiro = false;
        s_melhor = s;
        cout << " Custo: " << s_melhor.custo() << endl;
        p.registraCusto(s_melhor.custo());
      }
      if(p.parar()) break;
    }
    return s_melhor;
  }
