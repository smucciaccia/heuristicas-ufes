#ifndef H_ITERATED_LOCAL_SEARCH
#define H_ITERATED_LOCAL_SEARCH

#include "cvrp.hpp"
#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include <functional>
using namespace std;

class IteratedLocalSearch
{
  private:
    mutex mtx;
    Solucao s_melhor;
    int minimosLocais=0;
    int numThreads=4;
    int tamPerturbacao=7;

  public:
    IteratedLocalSearch();

    Solucao executa(Solucao& s_inicial, CriterioDeParada& criterioDeParada);
    Solucao executa(Grafo *g, CriterioDeParada& criterioDeParada);
    Solucao executa(Grafo *g);
};
#endif
