#ifndef H_GRASP
#define H_GRASP

#include "cvrp.hpp"
#include "HillClimbing.hpp"
#include <vector>
#include <set>
#include <math.h>
using namespace std;

class GRASP
{
  public:
    GRASP();
    int roleta(Grafo *g, int cidade, const multiset<int>& cidadesNaoVisitadas, int demanda, float alfa) ;
    Solucao andar(Grafo *g, float alfa);
    Solucao executa(Grafo *g, CriterioDeParada& p);
};
#endif
