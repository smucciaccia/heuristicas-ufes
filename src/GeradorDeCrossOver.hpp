#ifndef GERADOR_3
#define GERADOR_3

#include "cvrp.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>
using namespace std;

class GeradorDeCrossOver
{
  private:
    int tipo;

  public:
    GeradorDeCrossOver();
    Solucao pmx(Solucao p1, Solucao p2);

};
#endif
