#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "HillClimbing.hpp"
#include "VariableNeighborhoodSearch.hpp"
#include <vector>
using namespace std;

// class VariableNeighborhoodSearch -------------------------------------------------------------------

    VariableNeighborhoodSearch::VariableNeighborhoodSearch(){
      hillClimbing = HillClimbing("2-opt");
      gsv = GeradorDeSolucaoVizinha();
    }
 
    Solucao VariableNeighborhoodSearch::executaBasicVNS(Grafo *g){
      // int minimosLocais = 0;
      
      GeradorDeSolucaoInicial gsi;
      int max=1000;
      int niter=1;
      int vizinhanca = 1;      
      Solucao x = gsi.gerar(g);

      while (vizinhanca <= 3 && niter <= max)
      {        
        Solucao x1 = gerarPontoAleatorio(x, vizinhanca);
        Solucao x2 = hillClimbing.executa(x1); 

        // minimosLocais++;
        if(x2.custo() < x.custo() && x2.valida()) {
          // cout << "Mínimos locais pesquisados: " << minimosLocais++ << endl;
          x = x2;
          vizinhanca = 1;
          cout << "Solucao Atual: " << endl << x.toString() << endl;
        } else {            
          vizinhanca++; 
        }
        niter++;
      }

      cout << "Solucao Melhor: " << endl << x.toString() << endl;
      cout << "Custo: " << x.custo() << endl;
      return x;          
    }

    Solucao VariableNeighborhoodSearch::executaReducedVNS(Grafo *g){
      
      // cout << "RVNS" << endl;

      GeradorDeSolucaoInicial gsi;
      int max=1000000;
      int niter=1;
      int vizinhanca = 1;      
      Solucao x = gsi.gerar(g);

      while (vizinhanca <= 3 && niter <= max)
      {       
        Solucao x1 = gerarPontoAleatorio(x, vizinhanca);

        if(x1.custo() < x.custo() && x1.valida()) {
          x = x1;
          vizinhanca = 1;
          // cout << "Achou solução melhor" << endl << x.toString() << endl;
        } else {            
          vizinhanca++; 
        }
        niter++;
      }

      // cout << "Melhor Solucao Inicial: " << endl << x.toString() << endl;
      // cout << "Custo: " << x.custo() << endl;
      // cout << "----------------------" << endl;
      return x;          
    }

    Solucao VariableNeighborhoodSearch::executaGeneralVNS(Grafo *g){

      // cout << "GVNS" << endl;

      int max=1000000;
      int niter=1;
      int vizinhanca1 = 1;
      int vizinhanca2 = 1;
      Solucao x = executaReducedVNS(g);

      while (vizinhanca1 <= 3 && niter <= max)
      {       
        Solucao x1 = gerarPontoAleatorio(x, vizinhanca1);
        // Solucao x2 = x1;

        // Busca local por VND 
        while(vizinhanca2 <= 3)
        {
          // cout << "VND na vizinhança " << vizinhanca2 << endl;

          Solucao x2 = buscaLocal(x1, vizinhanca2);

          if(x2.custo() < x1.custo() && x2.valida()) {
            x1 = x2;
            vizinhanca2 = 1;
            // cout << "Achou solução melhor" << endl << x.toString() << endl;
          } else {            
            vizinhanca2++; 
          }          
        }

        if(x1.custo() < x.custo() && x1.valida()) {
          x = x1;
          vizinhanca1 = 1;
          // cout << "Achou solução melhor" << endl << x.toString() << endl;
          // cout << "Solucao Atual: " << endl << x.toString() << endl;
        } else {            
          vizinhanca1++; 
        }
        niter++;
      }

      cout << "Solucao Melhor: " << endl << x.toString() << endl;
      // cout << "Custo: " << x.custo() << endl;
      return x;          
    }

    Solucao VariableNeighborhoodSearch::gerarPontoAleatorio(Solucao s, int vizinhanca){

      int limite = s.getRota().size();
      int i = rand() % limite;
      int j = rand() % limite;

      switch (vizinhanca)
      {
      case 1:
        return gsv.v2opt(s, i, j);
        break;

      case 2:
      {
        int k = rand() % limite;
        int n = rand() % 4;
        return gsv.v3opt(s, i, j, k, n);
      }
        break;

      case 3:
        int k = rand() % limite;
        int l = rand() % limite;
        int n = rand() % 64;
        return gsv.v4opt(s, i, j, k, l, n);
        break;
      }
      return s;
    }

    Solucao VariableNeighborhoodSearch::buscaLocal(Solucao s, int vizinhanca){
      HillClimbing hc;
      Solucao vizinha = s;
      string modo;

      switch (vizinhanca)
      {
      case 1:
        modo = "2-opt";
        break;

      case 2:
        modo = "3-opt";
        break;
      
      case 3:
        modo = "4-opt";
        break;
      }

      hc = HillClimbing(modo);
      vizinha = hc.executa(s);
      return vizinha;
    }
