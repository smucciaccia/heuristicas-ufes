#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "GeradorDeCrossOver.hpp"
#include "HillClimbing.hpp"
#include "GeneticAlgorithm.hpp"
#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include <stddef.h>

//returns a float between 0 & 1
#define NUM_ALEAT      ((float) rand()/((float) RAND_MAX+1))

// class GeneticAlgorithm -------------------------------------------------------------------

        GeneticAlgorithm::GeneticAlgorithm(){
        }

        Solucao GeneticAlgorithm::executa(Grafo *g){
            return executa(g, 0);
        }

        Solucao GeneticAlgorithm::executa(Grafo *g, unsigned int alvo){

            // Medindo tempo de execu��o
            auto start = chrono::high_resolution_clock::now();

            Solucao populacao[tamanhoPop];          // Array da popula��o da gera��o atual
            Solucao nova_pop[tamanhoPop];           // Array auxiliar de populacao
            Solucao pais[2];                        // Array dos pais selecionados pelo m�todo 'selecao'
            Solucao filhos[2];                      // Array dos individuos geradas pela reprodu��o dos pais selecionados
            Solucao mutantes[2];                    // Array dos individuos ap�s muta��o

            int tamElite = round(tamanhoPop*taxaElitismo);
                                                    // N�mero de indiv�duos classificados como elite

            double metricas[3];                     // Array para c�lculo das m�tricas por geracao (melhor indiv, idx melhor, m�dia)

            GerarPop(g,populacao);                  // Gerando popula��o inicial:

            for (int g = 0; g<maxGeracoes; g++) {   // g: n�mero da gera��o atual

                cout << "\n\nGERACAO: " << g+1;
                for (int k = 0; k<tamanhoPop-tamElite; k = k + 2) {

                    //Selecao(pais,populacao,somaCumulativa);
                    SelecaoTorneio(pais,populacao);                 // selecionando dois entre toda a popula��o
                    Reproducao(filhos,pais,taxaReproducao);         // realizando cruzamento entre os pais, gerando os filhos
                    Mutacao(mutantes, filhos, taxaMutacao);         // processo de muta��o (nem sempre ocorrer�)

                    nova_pop[k]     = mutantes[0];                  // pr�xima gera��o ser� a armazenada no array nova_pop
                    nova_pop[k+1]   = mutantes[1];
                }
                Elitismo(populacao,nova_pop,taxaElitismo,tamElite); // mantendo a elite para pr�xima gera��o

                // calculando m�tricas(melhor individuo, idx do melhor, media da populacao)
                calcMetricas(populacao,metricas);
                cout << "\nMelhor: " << metricas[0] << "\tMedia: " << metricas[2];
                }

        auto end = chrono::high_resolution_clock::now();            // contabilizando tempo
        auto time_sec = chrono::duration_cast<chrono::seconds>(end - start);
        cout << "\nTempo: " << time_sec.count() << " segundos." << endl << endl;

        return populacao[int (metricas[1])];                        // retornando a melhor solu��o encontrada
        }

        Solucao *GeneticAlgorithm::GerarPop(Grafo *g, Solucao populacao[]) {
            // Gerando popula��o inicial:

            GeradorDeSolucaoInicial gsi;

            // Loop gerando solu��es iniciais aleat�rias
            for (int i=0; i<tamanhoPop; i++) {
                populacao[i] = gsi.gerar(g);
            }
            return populacao;
        }

        Solucao *GeneticAlgorithm::Selecao(Solucao array_pais[], Solucao populacao[],double somaCumulativa[]) {
            // Sele��o atrav�s do m�todo roleta viciada

            int indice1 = 0;
            int indice2 = 0;

            // Selecionando primeiro individuo
            for (int i = 0; i<tamanhoPop; i++) {
                // Analisando onde o valor aleatorio se encaixa na roleta (soma cumulativa)
                if (NUM_ALEAT < somaCumulativa[i]) {
                    indice1 = i;
                    break;
                }
            }

            // Selecionando segundo indiv�duo
            indice2 = indice1;
            while (indice2 == indice1) {
                // Analisando onde o valor aleatorio se encaixa na roleta (soma cumulativa)
                for (int i = 0; i<tamanhoPop; i++) {
                    if (NUM_ALEAT < somaCumulativa[i]) {
                        indice2 = i;
                        break;
                    }
                }
            }

            array_pais[0] = populacao[indice1];
            array_pais[1] = populacao[indice2];
            return array_pais;
        }

        Solucao *GeneticAlgorithm::SelecaoTorneio(Solucao array_pais[], Solucao populacao[]) {
            // Sele��o atrav�s do m�todo Torneio

            // Inicializa��o das vari�veis

            int indice1 = rand() % tamanhoPop;
            int indice2 = rand() % tamanhoPop;
            int idx_melhor1;
            int idx_melhor2;

            do {
                idx_melhor1 = indice1;
                idx_melhor2 = indice2;
                for (int i = 0; i<tamanhoTorneio; i++) {

                    if (populacao[idx_melhor1].custo()>populacao[indice1].custo()) {
                        idx_melhor1 = indice1;      // Vencedor sendo armazenado
                    }
                    indice1 = rand() % tamanhoPop;  // Selecionando pr�ximo oponente

                    if (populacao[idx_melhor2].custo()>populacao[indice2].custo()) {
                        idx_melhor2 = indice2;      // Vencedor sendo armazenado
                    }
                    indice2 = rand() % tamanhoPop;  // Selecionando pr�ximo oponente
                }
            } while(idx_melhor1 == idx_melhor2);    // Para gerar somente pais diferentes

            array_pais[0] = populacao[idx_melhor1];
            array_pais[1] = populacao[idx_melhor2];
            return array_pais;
        }

        Solucao *GeneticAlgorithm::Reproducao(Solucao array_filhos[], Solucao pais[], double taxaReproducao) {
            // Fun��o que recebe os pais selecionados e realiza o cruzamento entre eles
            // Inicialmente, o ponto de cross-over � aleat�rio a cada execu��o

            // Criando objeto da classe para gerar cross over
            GeradorDeCrossOver gco;

            // Gerando filhos pela fun��o
            //Solucao filho1, filho2;
            //array_filhos[0] = gco.pmx(pais[0],pais[1]);
            //array_filhos[1] = gco.pmx(pais[1],pais[0]);

            // Os filhos ser�o passados para a pr�xima gera��o caso o n�mero aleat�rio seja menor que a taxa de reprodu��o
            if (NUM_ALEAT > taxaReproducao) {
                array_filhos[0] = pais[0];
            } else {
                array_filhos[0] = gco.pmx(pais[0],pais[1]);
            }
            if (NUM_ALEAT > taxaReproducao) {
                array_filhos[1] = pais[1];
            } else {
                array_filhos[1] = gco.pmx(pais[1],pais[0]);
            }

            return array_filhos;
        }

        Solucao *GeneticAlgorithm::Mutacao(Solucao array_mutantes[], Solucao individuos[], double taxaMutacao) {
            // Recebe um array de solu��es e aplica a muta��o a depender de um fator aleat�rio r

            // Muta��o dependendo da taxa de muta��o
            if (NUM_ALEAT < taxaMutacao) {
                GeradorDeSolucaoVizinha gsv2opt;
                array_mutantes[0] = gsv2opt.randomMove2opt(individuos[0]);
            } else {
                array_mutantes[0] = individuos[0];
            }

            if (NUM_ALEAT < taxaMutacao) {
                GeradorDeSolucaoVizinha gsv2opt;
                array_mutantes[1] = gsv2opt.randomMove2opt(individuos[1]);
            } else {
                array_mutantes[1] = individuos[1];
            }

            return array_mutantes;
        }

        Solucao *GeneticAlgorithm::Elitismo(Solucao populacao[], Solucao nova_pop[], double taxaElitismo,int tamanhoElite) {
            // Fun��o que passa para a pr�xima gera��o os indiv�duos considerados elite

            // Ordenando somente os k melhores individuos
            kMaiores(populacao, tamanhoElite);

            // Pressupondo que seja recebida uma popula��o ordenada pela fun��o de custo
            for (int i = 0; i+tamanhoElite<tamanhoPop; i++) {
                // S�o mantidos os primeiros (tamanhoElite) elementos da popula��o anterior
                // e adicionados os elementos restantes da nova_pop at� que seja atingido o tamanho da popula��o
                populacao[i+tamanhoElite] = nova_pop[i];
            }

            return populacao;
        }

        double *GeneticAlgorithm::calcSomaCumulativa(double somaCumulativa[], Solucao populacao[]){
            // Fun��o para c�lculo da soma cumulativa (utilizada na sele��o por roleta viciada)

            // Ordenando popula��o pelo custo de seus indiv�duos
            sort(&populacao[0],&populacao[tamanhoPop-1],[](const Solucao indiv1,const Solucao indiv2)->bool{
                 return indiv1.custo() < indiv2.custo();
            });

            double custoPop = 0;                    // custoPop � a soma dos custos de cada indiv�duo da popula��o
            double custoNormalizado[tamanhoPop];    // array de custo normalizado

            // Normalizando custo dos indiv�duos da populacao
            for (int i = 0; i<tamanhoPop; i++) {
                // como � um problema de minimiza��o, o custo � passado como (1/custo)
                custoPop = custoPop + (1/double(populacao[i].custo()));
            }

            // Dividindo cada custo pela soma dos custos de toda a popula��o
            custoNormalizado[0] = (1/double(populacao[0].custo()))/custoPop;
            somaCumulativa[0] = custoNormalizado[0];
            for (int i = 1; i<tamanhoPop; i++) {
                custoNormalizado[i] = (1/double(populacao[i].custo()))/custoPop;
                somaCumulativa[i] = somaCumulativa[i-1] + custoNormalizado[i];
            }
            return somaCumulativa;
        }

        double *GeneticAlgorithm::calcMetricas(Solucao populacao[],double array_metricas[]){
            // Calcula melhor individuo, seu indice na populacao e a media da populacao

            double soma = 0;
            double melhor = populacao[0].custo();
            double media;
            double idx_melhor = 0;

            for (int i = 0; i<tamanhoPop; i++) {
                if (populacao[i].custo()<melhor){
                    melhor = populacao[i].custo();
                    idx_melhor = i;
                }
                soma = soma + populacao[i].custo();
            }
            media = soma/tamanhoPop;
            //cout << "\nMelhor: " << melhor << "\tMedia: " << media << endl;

            array_metricas[0] = melhor;
            array_metricas[1] = idx_melhor;
            array_metricas[2] = media;
            return array_metricas;
        }

        int *GeneticAlgorithm::kMaiores(Solucao populacao[],int k){
            // Utilizada no Elitismo
            // Coloca os k melhores individuos nas primeiras posi��es da popula��o
            quick_select_criada(populacao, 0, tamanhoPop-1,k);
        }

        Solucao GeneticAlgorithm::quick_select_criada(Solucao* input, int p, int r, int k){
            // input: vetor de inteiros
            // k: n�mero de valores q ser�o extra�dos
            // p: inicio do vetor?
            // r: ponteiro para o pivot?

            Solucao sol_pivot = input[r];
            int pivot = sol_pivot.custo();

            if ( p == r ) return input[p];
            int j = partition_criada(input, p, r, pivot);
            int length = j - p + 1;
            if ( length == k ) return input[j];
            else if ( k < length ) return quick_select_criada(input, p, j - 1, k);
            else  return quick_select_criada(input, j + 1, r, k - length);
}

        int GeneticAlgorithm::partition_criada(Solucao* input, int p, int r, int pivot){
            // input: vetor de inteiros
            // k: n�mero de valores q ser�o extra�dos
            // p: inicio do vetor?
            // r: ponteiro para o pivot?

            while ( p < r )
            {
                while ( input[p].custo() < pivot )
                    p++;

                while ( input[r].custo() > pivot )
                    r--;

                if ( input[p].custo() == input[r].custo() )
                    p++;
                else if ( p < r ) {
                    Solucao tmp = input[p];
                    input[p] = input[r];
                    input[r] = tmp;
                }
            }

            return r;
        }
