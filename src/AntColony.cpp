#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "HillClimbing.hpp"
#include "AntColony.hpp"
#include <vector>
#include <set>
#include <math.h>
using namespace std;

// class MatrizDeFeromonio -------------------------------------------------------------------

  MatrizDeFeromonio::MatrizDeFeromonio(Grafo *g, float intensidadeFeromonio) {
    iniciaMatriz(g, intensidadeFeromonio);
  }

  void MatrizDeFeromonio::iniciaMatriz(Grafo *g, float intensidadeFeromonio) {
    int dimensao = g->getNumVertices();
    float ferInicial = (10 * intensidadeFeromonio)/dimensao;
    this->tau = vector<vector<float>>(dimensao);
    for(int i = 0; i < this->tau.size(); i++)
    {
        this->tau[i] = vector<float>(dimensao);
        for(int j = 0; j < dimensao; j++)
        {
            if(i != j)
            {
                this->tau[i][j] = ferInicial;
            }
            else
            {
                this->tau[i][j] = 0;
            }
        }
    }
  }

  int MatrizDeFeromonio::roleta(Grafo *g, int cidade, const multiset<int>& cidadesNaoVisitadas, float alfa, float beta) {
    int i, n;
    float soma=0;
    for (i=0; i<tau[cidade].size(); i++) 
      if (i!=cidade && cidadesNaoVisitadas.count(i)>0)
        soma += pow(tau[cidade][i], alfa) * pow(g->distancia(cidade, i), -beta);

    float p = soma * ((float) rand() / (float) RAND_MAX);
    for (i=0; i<tau[cidade].size() && p>=0; i++) {
      if (i!=cidade && cidadesNaoVisitadas.count(i)>0) {
        p -= pow(tau[cidade][i], alfa) * pow(g->distancia(cidade, i), -beta); 
        n=i;
      }
    }
    return n;
  }

  void MatrizDeFeromonio::atualiza(float q, const Solucao& s){
    float custo = (float) s.custo();
    vector<int> v = s.getRota();
    for (int i=0; i<v.size()-1; i++) {
      float inc = q/custo;
      tau[v[i]][v[i+1]] += inc;
      tau[v[i+1]][v[i]] += inc;
    }
  }
  
  void MatrizDeFeromonio::evapora(float p){
    for (int i=0; i<tau.size(); i++)
      for (int j=0; j<tau[i].size(); j++) {
        tau[i][j] *= p;
        //if(tau[i][j]<1) tau[i][j] = 1;
      }
  }

  string MatrizDeFeromonio::toString() const {
    stringstream ss;
    for (int i=0; i<tau.size(); i++) {
      for (int j=0; j<tau[i].size(); j++)
        ss << tau[i][j] << " ";
      ss << endl;
    }
    return ss.str();
  }



// class Formiga -------------------------------------------------------------------

    Formiga::Formiga(Grafo *g){
      for(int i=0; i<3; i++)
        cidadesNaoVisitadas.insert(0);
      for(int i=1; i<g->getNumVertices(); i++)
        cidadesNaoVisitadas.insert(i);
    }

    Solucao Formiga::andar(Grafo *g, MatrizDeFeromonio m, float alfa, float beta){
      int demanda=0; 
      cidadesVisitadas.push_back(0);
      while (!cidadesNaoVisitadas.empty()) {
        int cidade = m.roleta(g, cidadesVisitadas.back(), cidadesNaoVisitadas, alfa, beta);
        demanda += g->demanda(cidade);
        if(demanda > 100){
          cidadesVisitadas.push_back(0);
          demanda = g->demanda(cidade);
          if (cidadesNaoVisitadas.count(0) > 0)
            cidadesNaoVisitadas.erase(cidadesNaoVisitadas.find(0));
        }
        cidadesNaoVisitadas.erase(cidadesNaoVisitadas.find(cidade));
        cidadesVisitadas.push_back(cidade);
      } 
      cidadesVisitadas.push_back(0);
      return Solucao(g, cidadesVisitadas);
    }

// class AntColony -------------------------------------------------------------------

    AntColony::AntColony(){}

    Solucao AntColony::executa(Grafo *g, CriterioDeParada& p){
      int nformigas=20;
      float taxaEvaporacao=0.03;
      float intensidadeFeromonio=1;
      float alfa=1.2;
      float beta=1.2;
/*
      cout << "Digite o número de formigas: ";
      cin >> nformigas;
      cout << "Digite a taxa de evaporação: ";
      cin >> taxaEvaporacao;
      cout << "Digite a intensidade do feromonio: ";
      cin >> intensidadeFeromonio;
      cout << "Digite um valor para alfa: ";
      cin >> alfa;
      cout << "Digite um valor para beta: ";
      cin >> beta;
*/
      MatrizDeFeromonio m(g, intensidadeFeromonio);

      Solucao s_melhor;
      bool primeiro=true;
      vector<Solucao> vs(nformigas);
      int nIteracao=0; 
      while (true) {
        nIteracao++;
        for (int i=0; i<nformigas; i++) {
          Formiga f(g);
          Solucao s = f.andar(g, m, alfa, beta);
          vs[i] = s;

          if (primeiro || s.custo() < s_melhor.custo()) {
            s_melhor = s;
            primeiro = false;
            //cout << "Iteracao: " << nIteracao << " Custo: " << s_melhor.custo() << endl;
            p.registraCusto(s_melhor.custo());
          }
        }

        m.evapora(1 - taxaEvaporacao);
        //m.atualiza(intensidadeFeromonio, s_melhor);
        for(int n=0; n<nformigas; n++)
          m.atualiza(intensidadeFeromonio, vs[n]);

        if(p.parar()) break;
      }

      return s_melhor;
    }
