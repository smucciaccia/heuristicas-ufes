#ifndef GERADOR_2 
#define GERADOR_2 

#include "cvrp.hpp"
#include <vector>
using namespace std;

class GeradorDeSolucaoVizinha
{
  private:
    int modo=0;

  public:
  GeradorDeSolucaoVizinha();

  GeradorDeSolucaoVizinha(string tipo);

/* Retorna um vizinho com um custo menor que a solução atual.
 * Caso exista um vizinho com um custo menor retorna true e coloca o vizinho
 * encontrado na variável do segundo parâmetro.
 *
 * Entrada: 
 *   (Solucao) Solução atual
 * Saídas: 
 *   (bool) true se conseguiu encontrar uma solução melhor, false caso seja um mínimo local
 *   (Solucao&) A solução melhor encontrada, se não for encontrada não modifica a variável
 */
  bool melhorarSolucao(Solucao s_atual, Solucao& s_melhor);

  Solucao randomMove2opt(Solucao& s_atual);

  Solucao v2opt(Solucao sol, int i, int j);
  
  bool dif2opt(const Solucao sol, int i, int j);

  int preCusto2opt(Grafo *g, const vector<int>& rota, int i, int j) const;

  bool preValidade2opt(Grafo *g, const vector<int>& rota, int ci, int cj) const;

  Solucao v3opt(Solucao& sol, int i, int j, int k, int n);

  Solucao v4opt(Solucao& sol, int i, int j, int k, int l, int n);

private:

  bool melhorar2opt(Solucao& sol, Solucao& melhor);

  bool melhorar3opt(Solucao& sol, Solucao& melhor);

  bool melhorar4opt(Solucao& sol, Solucao& melhor);

};

class GeradorDeSolucaoVizinha2opt
{
  public:
  GeradorDeSolucaoVizinha2opt();

  bool firstGain(Solucao& sol, Solucao& melhor);

  void randomMove(Solucao& s_atual);

  void randomMove(Solucao& s_atual, int n);

  void move(Solucao& sol, int i, int j);
  
  bool dif(const Solucao& sol, int i, int j);

  int preCusto(Grafo *g, const vector<int>& rota, int i, int j) const;

  bool preValidade(Grafo *g, const vector<int>& rota, int ci, int cj) const;

  bool preValidade(Solucao& sol, int ci, int cj) const;

};
#endif
