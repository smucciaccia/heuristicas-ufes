#ifndef H_ANT_COLONY
#define H_ANT_COLONY

#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "HillClimbing.hpp"
#include <vector>
#include <set>
#include <math.h>
using namespace std;

class MatrizDeFeromonio
{
  private:
    vector<vector<float>> tau;

  public: 
    MatrizDeFeromonio(Grafo *g, float intensidadeFeromonio) ;

    void iniciaMatriz(Grafo *g, float intensidadeFeromonio) ;

    int roleta(Grafo *g, int cidade, const multiset<int>& cidadesNaoVisitadas, float alfa, float beta) ;

    void atualiza(float q, const Solucao& s);
    
    void evapora(float p);

    string toString() const ;
};


class Formiga
{
  private:
    vector<int> cidadesVisitadas;
    multiset<int> cidadesNaoVisitadas;

  public: 
    Formiga(Grafo *g);
    Solucao andar(Grafo *g, MatrizDeFeromonio m, float alfa, float beta);
};

class AntColony
{
  public:
    AntColony();
    Solucao executa(Grafo *g, CriterioDeParada& p);
};
#endif
