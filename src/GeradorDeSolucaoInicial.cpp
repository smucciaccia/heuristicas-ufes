#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>
using namespace std;

// class GeradorDeSolucaoInicial ------------------------------------------------------------------

  int tipo;

  GeradorDeSolucaoInicial::GeradorDeSolucaoInicial(){
    tipo=0;
  }

  GeradorDeSolucaoInicial::GeradorDeSolucaoInicial(string stipo){
    if (stipo.compare("random")) tipo=1;
    if (stipo.compare("iterated")) tipo=2;
    if (stipo.compare("grasp")) tipo=3;
    else tipo=0;
  }

  Solucao GeradorDeSolucaoInicial::gerar(Grafo *g){
      return random(g);
  }

  Solucao GeradorDeSolucaoInicial::random(Grafo *g){
    vector<int> vetorRandom;
    for (int i=1; i<g->getNumVertices(); i++)
      vetorRandom.push_back(i);
    random_shuffle(vetorRandom.begin(), vetorRandom.end());

    int capacidade = 0;
    vector<int> rota;
    rota.push_back(0);
    for (int i=0; i<vetorRandom.size(); i++) {
      capacidade += g->demanda(vetorRandom[i]);
      if (capacidade > 100) {
        rota.push_back(0);
        capacidade = g->demanda(vetorRandom[i]);
      }
      rota.push_back(vetorRandom[i]);
    }
    rota.push_back(0);
    return Solucao(g, rota);
  }
