#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "cvrp.hpp"
#include "HillClimbing.hpp"
#include "IteratedLocalSearch.hpp"
#include "VariableNeighborhoodSearch.hpp"
#include "GeneticAlgorithm.hpp"
#include "SimulatedAnnealing.hpp"
#include "AntColony.hpp"
#include "GRASP.hpp"
#include <stdio.h>
#include <list>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <dirent.h>
#include <sys/types.h>
using namespace std;

string dir_problemas, dir_solucoes;

vector<string> listarArquivosNoDiretorio(string diretorio){
  vector<string> arquivos;
  DIR *dir;
  struct dirent *ent;
  dir = opendir(diretorio.c_str());
  if (dir != NULL) {
    while ((ent = readdir(dir)) != NULL) {
      string nomeDiretorio(ent->d_name);
      if (nomeDiretorio.compare(".") != 0 && nomeDiretorio.compare("..") != 0 ) {
        arquivos.push_back(nomeDiretorio);
      }
    }
    closedir (dir);
  } else {
    cerr << "Erro ao abrir o diretório " << diretorio;
  }
  sort(arquivos.begin(), arquivos.end());
  return arquivos;
}

int obterOpcaoDoUsuario(){
  int opcao;
  cout << "Opções:" << endl;
  cout << "  0 Obter uma solução para um problema CVRP." << endl;
  cout << "  1 Obter uma solução para um problema CVRP a partir de uma solução inicial." << endl;
  cout << "  2 Obter soluções para todos os problemas da pasta ./problems/." << endl;
  cout << "  3 Analisar o custo e as demandas de uma solução obtida." << endl;
  cout << "  4 Visualizar o plot de uma solução (requer opengl)." << endl;
  cout << "  5 Realizar um benchmark e medir o tempo de execução do algoritmo." << endl;
  cout << "  6 Obter um Lower Bound para um problema CVRP." << endl;
  cout << "  7 Comparar as arestas de duas soluções." << endl;
  cout << "Digite o número da opção desejada: ";
  cin >> opcao;
  cout << endl;
  return opcao;
}

string obterProblemaDoUsuario(){
  string problem;
  int n_problema;
  vector<string> problemas = listarArquivosNoDiretorio(dir_problemas);
  cout << "Problemas: " << endl;
  for(int i=0; i< problemas.size(); i++){
    cout << "  " << i << " " << problemas[i] << endl;
  }
  cout << "Digite o número do problema desejado: ";
  cin >> n_problema;
  cout << endl;
  problem.append(dir_problemas);
  problem.append(problemas[n_problema]);
  return problem;
}

string obterSolucaoDoUsuario(){
  string solution;
  vector<string> solucoes = listarArquivosNoDiretorio(dir_solucoes);
  cout << "Soluções: " << endl;
  for(int i=0; i< solucoes.size(); i++){
    cout << "  " << i << " " << solucoes[i] << endl;
  }
  int n_solucao;
  cout << "Digite o número da solução desejada: ";
  cin >> n_solucao;
  cout << endl;
  solution.append(dir_solucoes);
  solution.append(solucoes[n_solucao]);
  return solution;
}

int obterAlgoritmoDoUsuario(){
  int n_algoritmo;
  cout << "Algoritmos: " << endl;
  cout << "  0 Hill Climbing (HC)." << endl;
  cout << "  1 Iterated Local Search (ILS)." << endl;
  cout << "  2 Variable Neighborhood Search (VNS)." << endl;
  cout << "  3 Genetic Algorithm (GA)." << endl;
  cout << "  4 Simulated Annealing (SA)." << endl;
  cout << "  5 Ant Colony (AC)." << endl;
  cout << "  6 Greedy Randomized Adaptative Search Procedure (GRASP)." << endl;
  cout << "  7 Tabu Search (TS)." << endl;
  cout << "  8 Biased Key Random Genetic Algorithm (BKRGA)." << endl;
  cout << "  9 Particle Swarm Optimization (PSO)." << endl;
  cout << " 10 Differential Evolution (DE)." << endl;
  cout << " 11 Clustering Search (CS)." << endl;
  cout << " 12 GRASP with Path Relinking (GRASPPR)." << endl;
  cout << "Digite o número do algoritmo a ser usado: ";
  cin >> n_algoritmo;
  cout << endl;
  return n_algoritmo;
}

Solucao executa(Grafo *grafo, int n_algoritmo, CriterioDeParada& p) {
  Solucao s;
  if (n_algoritmo == 0) {
    p.start();
    HillClimbing h;
    s = h.executa2opt(grafo);
    p.stop();
  }
  else if (n_algoritmo == 1) {
    p.start();
    IteratedLocalSearch h;
    s = h.executa(grafo, p);
    p.stop();
  }
  else if (n_algoritmo == 2) {
    VariableNeighborhoodSearch vns;
    s = vns.executaGeneralVNS(grafo);
  }
  else if (n_algoritmo == 3) {
    p.start();
    GeneticAlgorithm ga;
    s = ga.executa(grafo);
    p.stop();
  }
  else if (n_algoritmo == 4) {
    p.start();
    SimulatedAnnealing sa;
    s = sa.executa(grafo, p);
    p.stop();
  } 
  else if (n_algoritmo == 5) {
    p.start();
    AntColony ac;
    s = ac.executa(grafo, p);
    p.stop();
  } 
  else if (n_algoritmo == 6) {
    p.start();
    GRASP gr;
    s = gr.executa(grafo, p);
    p.stop();
  } 
  else if (n_algoritmo < 13) {
    cout << "Algoritmo ainda não implementado." << endl;
  }
  else {
    cout << "Este número não é uma opção." << endl;
  }
  return s;
}

Solucao executa(Solucao s_inicial, int n_algoritmo, CriterioDeParada& p) {
  Solucao s;
  if (n_algoritmo == 0) {
    p.start();
    HillClimbing h;
    s = s_inicial;
    h.executa2opt(s);
    p.stop();
  }
  else if (n_algoritmo == 1) {
    p.start();
    IteratedLocalSearch h;
    s = h.executa(s_inicial, p);
    p.stop();
  }
  else if (n_algoritmo < 13) {
    cout << "Algoritmo ainda não implementado." << endl;
  }
  else {
    cout << "Este número não é uma opção." << endl;
  }
  return s;
}

void mainLoop () {

  int opcao = obterOpcaoDoUsuario();

  if (opcao == 0) {
    string problema = obterProblemaDoUsuario();
    Grafo *grafo = new Grafo(problema);
    int n_algoritmo = obterAlgoritmoDoUsuario();
    CriterioDeParada p; 
    Solucao s = executa(grafo, n_algoritmo, p);

    cout << "Solução Final: " << endl << s.toString() << endl;
    cout << "Tempo gasto na execução: " << p.tempoSegundos() << " segundos." << endl << endl;

    int n=0;
    string nomeSol;
    do {
      n++;
      nomeSol.clear();
      nomeSol.append(dir_solucoes);
      nomeSol.append(problema.substr(problema.rfind("/"), problema.rfind(".vrp")-problema.rfind("/")));
      nomeSol.append("-c").append(to_string(s.custo()));
      if(n!=1) nomeSol.append("-").append(to_string(n));
      nomeSol.append(".sol");
    } while (ifstream(nomeSol.c_str()).good());
    ofstream log;
    log.open(nomeSol);
    if(!log){
      cerr << "Não foi possível abrir o arquivo para escrever a solução!" << endl;
      exit(1);
    }
    log << s.toString();
    log.close();
  }

  if (opcao == 1) {
    string problema = obterProblemaDoUsuario();
    Grafo *grafo = new Grafo(problema);

    string dir_solucao = obterSolucaoDoUsuario();
    Solucao s_inicial = Solucao(grafo, dir_solucao);

    int n_algoritmo = obterAlgoritmoDoUsuario();
    CriterioDeParada p; 
    p.setTempoLimite(28800);
    Solucao s = executa(s_inicial, n_algoritmo, p);

    cout << "Solução Final: " << endl << s.toString() << endl;
    cout << "Tempo gasto na execução: " << p.tempoSegundos() << " segundos." << endl << endl;

    int n=0;
    string nomeSol;
    do {
      n++;
      nomeSol.clear();
      nomeSol.append(dir_solucoes);
      nomeSol.append(problema.substr(11, problema.rfind(".vrp")-11));
      nomeSol.append("-c").append(to_string(s.custo()));
      if(n!=1) nomeSol.append("-").append(to_string(n));
      nomeSol.append(".sol");
    } while (ifstream(nomeSol.c_str()).good());
    ofstream log;
    log.open(nomeSol);
    if(!log){
      cerr << "Não foi possível abrir o arquivo para escrever a solução!" << endl;
      exit(1);
    }
    log << s.toString();
    log.close();
  }

  if (opcao == 2) {
    Logger log;
    int n_algoritmo = obterAlgoritmoDoUsuario();
    vector<string> problemas = listarArquivosNoDiretorio(dir_problemas);

    int vezes, tempo;
    cout << "Executar quantas vezes em cada problema? ";
    cin >> vezes;
    cout << "Executar por quantos segundos em cada vez? ";
    cin >> tempo;

    for(int i=0; i<problemas.size(); i++){
      string problem;
      problem.append(dir_problemas);
      problem.append(problemas[i]);
      Grafo *grafo = new Grafo(problem);
      Solucao s;
      log.registra(problem);

      for(int j=0; j<vezes; j++){
        cout << "--------------------------------------------------------------------------------" << endl;
        cout << "Problema " << problemas[i] << ", execucao " << j <<  endl;

        CriterioDeParada p;
        p.setTempoLimite(tempo);
        s = executa(grafo, n_algoritmo, p);
        log.registra(grafo, s.custo(), p.tempoCustoSegundos());
        s.acertarZerosRepetidos();

        cout << endl << s.toString() << endl;
        cout << "Tempo gasto na execução: " << p.tempoSegundos() << " segundos." << endl << endl;
      }
    }
    cout << "Soma total das diferenças em relação ao melhor custo: " << log.soma << endl << endl;
  }

  if (opcao == 3) {
    string dir_problema = obterProblemaDoUsuario();
    Grafo *grafo = new Grafo(dir_problema);

    string dir_solucao = obterSolucaoDoUsuario();
    Solucao s = Solucao(grafo, dir_solucao);
    Solucao s2 = Solucao(grafo, dir_solucao);

    cout << "Solução analisada: " << endl;
    cout << s.analisa() << endl;
    cout << "Solução válida? " << endl;
    cout << (s.valida() ? "Sim" : "Não") << endl << endl;
    cout << "Forma interna: " << endl;
    cout << s.serialize() << endl << endl;

    GeradorDeSolucaoVizinha gsi2opt("2-opt");
    bool minimo2opt = gsi2opt.melhorarSolucao(s, s2);
    cout << "É um mínimo local 2-opt? " << (minimo2opt ? "Não" : "Sim") << endl;

    GeradorDeSolucaoVizinha gsi3opt("3-opt");
    bool minimo3opt = gsi3opt.melhorarSolucao(s, s2);
    cout << "É um mínimo local 3-opt? " << (minimo3opt ? "Não" : "Sim") << endl;

    GeradorDeSolucaoVizinha gsi4opt("4-opt");
    bool minimo4opt = gsi4opt.melhorarSolucao(s, s2);
    cout << "É um mínimo local 4-opt? " << (minimo4opt ? "Não" : "Sim") << endl << endl;
  }

  if (opcao == 4) {
    string dir_problema = obterProblemaDoUsuario();
    string dir_solucao = obterSolucaoDoUsuario();

    string comando("./plot-solutions/plot-solutions");
    comando.append(" ");
    comando.append(dir_problema);
    comando.append(" ");
    comando.append(dir_solucao);
    cout << comando << endl;
    system(comando.c_str());
    cout << endl;
  }

  if (opcao == 5) {
    srand( 1 );
    string problema = obterProblemaDoUsuario();
    Grafo *grafo = new Grafo(problema);
    int n_algoritmo = obterAlgoritmoDoUsuario();
    CriterioDeParada p; 
    Solucao s = executa(grafo, n_algoritmo, p);

    cout << "Solução Final: " << endl << s.toString() << endl;
    cout << "Tempo gasto na execução: " << p.tempoSegundos() << " segundos." << endl << endl;
  }

  if (opcao == 6) {
    string problema = obterProblemaDoUsuario();
    Grafo *grafo = new Grafo(problema);

    CalculadorDeLowerBound clb;
    float lb = clb.executa(grafo);
    cout << "Lower Bound Calculado: " << lb << endl << endl;
  }

  if (opcao == 7) {
    string dir_problema = obterProblemaDoUsuario();
    Grafo *grafo = new Grafo(dir_problema);
    string dir_solucao = obterSolucaoDoUsuario();
    Solucao s1 = Solucao(grafo, dir_solucao);
    string dir_solucao2 = obterSolucaoDoUsuario();
    Solucao s2 = Solucao(grafo, dir_solucao2);
    cout << "Número de arestas em uma solução que não existe na outra: " << s1.compare(s2) << endl;
    cout << "Número de arestas que existe nas duas soluções: " << s1.similaridade(s2) << endl << endl;
  }
}

int main(int argc, char** argv)
{
  srand( time(NULL) );

  dir_problemas = string("./problems/");
  dir_solucoes = string("./solutions/");

  for(int i=1; i<argc; i++){
    if (string(argv[i-1]).compare("-p")==0)
      dir_problemas = string(argv[i]);
    if (string(argv[i-1]).compare("-s")==0)
      dir_solucoes = string(argv[i]);
  }

  while(true){
    mainLoop();
  }
  return 0;
}

