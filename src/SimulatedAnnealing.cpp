#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "SimulatedAnnealing.hpp"
#include <vector>
#include <math.h>
using namespace std;

float resfriamentoLinear(float temperaturaInicial, int iteracao, int max_iteracoes){
  float fiter = (float) iteracao;
  float fmaxiter = (float) max_iteracoes;
  return temperaturaInicial * (fmaxiter - fiter) / fmaxiter;
}

float determinarTemperaturaInicial(Grafo *g, float X0){
  GeradorDeSolucaoInicial gsi;
  GeradorDeSolucaoVizinha gsv;
  float Xn=0, Tn=1, erro=0.02;
  do {
    Solucao s_inicial = gsi.gerar(g);
    Solucao s_atual = Solucao(s_inicial);
    int niter = 2000;
    int conta = 0;
    for(int i=0; i<niter; i++){
      Solucao s_i = gsv.randomMove2opt(s_atual);
      float temp = resfriamentoLinear(Tn, 0, niter);
      float frandom = (float) rand() / (float) RAND_MAX;
      float delta = s_atual.custo()-s_i.custo();
      if ( s_i.custo() < s_atual.custo() || exp( delta / temp) > frandom ) {
        conta++;
        s_atual = s_i;
      }
    } 
    Xn = (float) conta / (float) niter;
    Tn = Tn * log( Xn ) / log( X0 );
  } while (Xn < X0-erro || Xn > X0+erro);
  return Tn;
}

// class SimulatedAnnealing -------------------------------------------------------------------

    SimulatedAnnealing::SimulatedAnnealing(){}

    Solucao SimulatedAnnealing::executa(Grafo *g, CriterioDeParada& p){
      cout << "Calculando temperatura inicial... ";
      cout.flush();
      float temperaturaInicial = determinarTemperaturaInicial(g, 0.5);
      cout << "\r\e[0K" << "Temperatura Inicial: " << temperaturaInicial << endl;

      int niter = 1000000;
      int permil = niter/1000, cont = permil;
      GeradorDeSolucaoInicial gsi;
      GeradorDeSolucaoVizinha2opt gsv;
      Solucao s_inicial = gsi.gerar(g);
      Solucao s_atual = Solucao(s_inicial);
      Solucao s_melhor = Solucao(s_inicial);

      int custoAtual = s_atual.custo();
      int custoMelhor = s_melhor.custo();

      for(int i=0; i<niter; i++){
        cont++;
        if(cont > permil){
          cout << "\r\e[0K" << "Progresso: " << ((1+i)/permil)/10 << "." << ((1+i)/permil)%10 << "%";
          cout << " Melhor Custo: " << custoMelhor << " Custo Atual: " << custoAtual;
          cout.flush();
          cont=0;
        }

        int tam = s_atual.getRota().size()-1;
        int a1, a2;
        do {
          a1 = rand() % (tam-2);
          a2 = a1+2 + rand() % (tam-a1-2);
        } while( !gsv.preValidade(s_atual.getGrafo(), s_atual.getRota(), a1, a2));

        float temp = resfriamentoLinear(temperaturaInicial, i, niter);
        float frandom = (float) rand() / (float) RAND_MAX;
        float delta = gsv.preCusto(s_atual.getGrafo(), s_atual.getRota(), a1, a2);

        if ( delta > 0 || exp( delta / temp) > frandom ) {
          gsv.move(s_atual, a1, a2);
          custoAtual -= delta;
          if (custoAtual < custoMelhor) {
            s_melhor = s_atual;
            custoMelhor = custoAtual;
            p.registraCusto(custoMelhor);
          }
        }
      } 
      cout << endl << endl;
      s_melhor.acertarZerosRepetidos();
      return s_melhor;
    }

// class SimulatedAnnealingViz -------------------------------------------------------------------

    SimulatedAnnealingViz::SimulatedAnnealingViz(){}

    Solucao SimulatedAnnealingViz::executa(Grafo *g, CriterioDeParada& p){
      float temperaturaInicial = determinarTemperaturaInicial(g, 0.5);
      int niter = 200000000;
      GeradorDeSolucaoInicial gsi;
      GeradorDeSolucaoVizinha gsv;
      Solucao s_inicial = gsi.gerar(g);
      Solucao s_atual = Solucao(s_inicial);
      Solucao s_melhor = Solucao(s_inicial);
      for(int i=0; i<niter; i++){
        Solucao s_i = gsv.randomMove2opt(s_atual);
        float temp = resfriamentoLinear(temperaturaInicial, i, niter);
        float frandom = (float) rand() / (float) RAND_MAX;
        float delta = s_atual.custo()-s_i.custo();
        if ( s_i.custo() < s_atual.custo() || exp( delta / temp) > frandom ) {
          s_atual = s_i;
          if (s_atual.custo() < s_melhor.custo()) {
            s_melhor = s_atual;
            p.registraCusto(s_melhor.custo());
          }
        }
      } 
      return s_melhor;
    }
