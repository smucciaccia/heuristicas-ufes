#ifndef H_VARIABLE_NEIGHBORHOODSEARCH_SEARCH
#define H_VARIABLE_NEIGHBORHOODSEARCH_SEARCH

#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include "HillClimbing.hpp"
#include <vector>
using namespace std;

class VariableNeighborhoodSearch
{
  private:
    HillClimbing hillClimbing;
    GeradorDeSolucaoVizinha gsv;

  public:
    VariableNeighborhoodSearch();
    Solucao executaBasicVNS(Grafo *g);
    Solucao executaReducedVNS(Grafo *g);
    Solucao executaGeneralVNS(Grafo *g);

  private:
    Solucao gerarPontoAleatorio(Solucao s, int vizinhanca);
    Solucao buscaLocal(Solucao s, int vizinhanca);
};
#endif
