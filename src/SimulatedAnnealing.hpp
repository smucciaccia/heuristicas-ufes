#ifndef H_SIMULATED_ANNEALING
#define H_SIMULATED_ANNEALING

#include "cvrp.hpp"
#include "GeradorDeSolucaoInicial.hpp"
#include "GeradorDeSolucaoVizinha.hpp"
#include <vector>
#include <math.h>
using namespace std;

float resfriamentoLinear(float temperaturaInicial, int iteracao, int max_iteracoes);

float determinarTemperaturaInicial(Grafo *g, float X0);

class SimulatedAnnealing
{
  public:
    SimulatedAnnealing();

    Solucao executa(Grafo *g, CriterioDeParada& p);
};

class SimulatedAnnealingViz
{
  public:
    SimulatedAnnealingViz();

    Solucao executa(Grafo *g, CriterioDeParada& p);
};
#endif
