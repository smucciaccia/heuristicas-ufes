#ifndef GERADOR_1
#define GERADOR_1 

#include "cvrp.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>
using namespace std;

class GeradorDeSolucaoInicial
{
  int tipo;

  public:
    GeradorDeSolucaoInicial();

    GeradorDeSolucaoInicial(string stipo);

    Solucao gerar(Grafo *g);

    Solucao random(Grafo *g);
};
#endif
